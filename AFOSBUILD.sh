rm -rf /opt/ANDRAX/on-the-fly

source /opt/ANDRAX/PYENV/python3/bin/activate

/opt/ANDRAX/PYENV/python3/bin/pip3 install -r requirements.txt
/opt/ANDRAX/PYENV/python3/bin/pip3 install netfilterqueue

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Pip install requirements... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf $(pwd) /opt/ANDRAX/on-the-fly

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
